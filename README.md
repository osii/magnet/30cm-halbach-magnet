# Permanent low-field MR Magnet

## System Specifications

- Permanent magnet in "ROMA" (Rotation Optimized Magnet) array, which differs from previously used Halbach-based magnet designs (v1*) in that the orientation of the individual magnets inside the array no longer follow the strict Halbach definition but rather are optimized to maximize homogeneity
- 1936× (12×12×12)mm³ NdFeBr N52 magnets
- 384× (50×12×12)mm³ NdFeBr N52 magnets
- B<sub>0</sub> ~ 50mT
- Weight ~100kg
- Bore = ø322mm × 420+14mm (with back plate, without gradients)
- outer dimensions (l×b×h) = (434×520×567)mm³

![](/res/img/nomenclature.png){width=30%}\
_Coordinate system and B<sub>0</sub> direction_

## Repository Overview

The repository follows a Unixish directory structure, inspired by the [Minimal OSH Repository Template](https://gitlab.fabcity.hamburg/software/template-osh-repo-structure-minimal/-/blob/main/mod/unixish/README.md):

- [doc](/doc/): documentation
	- [Manufacturing Guide](manufacturing-guide.md)
	- [Assembly Guide](assembly-guide.md)
	- [Quality Management Guide](qm-guide.md)
- [res](/res/): additional resources used and referenced throughout the repository, e.g. images and datasheets
- [src](/src/): source files

## Contributing

### CAD files via Git LFS

SolidEdge files (`.par` and `.asm`) and FreeCAD files (`.FCStd`) are mounted via [Git LFS](https://git-lfs.com/); for more information see [this GitLab-Wiki article on how to use it](https://docs.gitlab.com/ee/topics/git/lfs/) and [this blog post](https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/) for basic understanding and background information.

### Branches

Active branches are:

- [`rc/1.0`](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/tree/rc/1.0)
	- latest stable release
	- built in PTB, Berlin
- [`rc/2.0`](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/tree/rc/2.0)
	- stronger B<sub>0</sub> field
	- shorter magnet (better for brain scans)
	- lids screwed (instead of glued)
	- spacer sleeves instead of (counter) nuts
	- rectangular shim trays
	- built for BwKh, Hamburg
- [`rc/2.1`](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/tree/rc/2.1)
	- B<sub>0</sub> now upright
	- built for [A4IM project](https://www.a4im.ptb.de/home)

For further details and background information please see the [document on OSI²'s branch management](https://gitlab.com/osii/cab/-/blob/main/TsDC/Contribution-Management.md#branches).

### Get in Touch

To join the development, feel free to

- join us on [Matrix](https://matrix.to/#/#osii:matrix.org),
- drop us an [issue](https://gitlab.com/osii/rf-system/rf-power-amplifier/1kw-peak-rfpa/-/issues) or
- file a merge request directly.

## Contributors (alphabetical order)

Martin Häuer, Tom O'Reilly, Sebastian Schachel, Wouter Teeuwisse, Lukas Winter

## Acknowledgements

The project (22HLT02 A4IM) has received funding from the European Partnership on Metrology, co-financed by the European Union's Horizon Europe Research and Innovation Programme and by the Participating States. 

This research is funded by dtec.bw-Digitalization and Technology Research Center of the Bundeswehr. dtec.bw is funded by the European Union - NextGeneration EU.

This work is supported by the Open Source Imaging Initiative (OSI²), https://www.opensourceimaging.org

## License and Liability

<!--- UPDATE LICENSES CC-BY for /res and /doc, GPLv3 for FreeCAD script, rest under CERN OHL -->

This is an open source hardware project licensed under the CERN Open Hardware Licence Version 2 - Weakly Reciprocal. For more information please check [LICENSE](LICENSE) and [DISCLAIMER](DISCLAIMER.pdf).
