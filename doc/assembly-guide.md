# Assembly Guide

## General

> This guide gives instructions on how to assemble the magnet. For manufacturing instructions of the parts, please refer to the [manufacturing guide](/doc/manufacturing-guide.md)

The magnet consists of rings that hold a number of permanent magnets in a specific orientation.
The rings have to be milled, equipped with permanent magnets, closed with a lid and then mounted in a defined order using brass rods.

## Populating the Rings

### Rings 1…8

Parts for this step:

```
R1
R2
R3
R4
R5
R6
R7
R8
RL
RL4
Mag12
M4hex12
```

For expectation management: calculate about 20…30min per ring.

1. Mark the polarity of all permanent magnets (`Mag12`) using any sort of reference magnet with known polarity:\
	![manually marked magnets](/res/img/magnet-mark-all-north.jpg){width=60%}\
	_manually marked magnets_
2. Before inserting the permanent magnets, make sure that the magnet pockets in the ring (`R1…8`) are clean and chip-free, see figure below\
	![detail of lint from milling](/res/img/lint-detail.jpg){width=30%}\
	_detail of lint from milling_
3. Insert the permanent magnets (`Mag12`) into the pockets of the ring `R1…8`.
	- **Make sure that all inserted magnets fulfill the quality requirements** (full field strength, no chipping etc.), otherwise you might end up with a severely inhomogeneous $B_0$-field later.\
	Please see [QM guide](/doc/qm-guide-md) for details.
	- The mark of the magnet pocket indicates north:\
		![detail of a magnet pocket](/res/img/magnet-pocket.jpg){width=30%}\
		_detail of a magnet pocket_
    - It's generally easier to first fill the inner ring of magnets before starting with the outer one. Convenient start and end positions are where one of the magnet poles points towards the center of the ring.
	    - We're talking 1936 permanent magnets in total for 2×8 rings. Please don't use your bare fingers for inserting them :)\
	    ![inserting magnets 1](/res/img/insert-magnets1.jpg){width=30%} ![inserting magnets 2](/res/img/insert-magnets2.jpg){width=30%}\
	    _inserting magnets_
	    - Watch your fingers!\
	    	![magnets trying to unmount themselves when inserting the next](/res/img/magnet-lift.jpg){width=30%}\
	    	_magnets trying to unmount themselves when inserting the next_
	    - Make your life easier with supporting tools, e.g. the [magnet stapler](https://gitlab.com/osii/tools/magnet-stapler).
	    - Putting the ring on a soft surface might be helpful to prevent impacts (when pushing new magnets in) from provoking mounted magnets to jump out again.
4. Quality check: Ensure the correct rotation of all magnets.\
	![Ring with incorrectly rotated magnet – can you spot it?](/res/img/finding-waldo.jpg){width=30%}\
	_Ring with incorrectly rotated magnet – can you spot it?_
5. Screw the lid (`RL`) on top of the ring (`R1…8`).
    - …using `M4hex12`:\
	    ![mounted ring lid](/res/img/mounted-lid.jpg){width=30%}\
	    _mounted ring lid_
	- **NOTE** to mount one `R8` with `RL4` and the other with `RL`, see the [magnet mounting step](#mounting-the-magnet) below.

Be aware of quality requirements while executing the steps above.
Any unrecognized flaw here can easily result in a lot of pain and work later on, since retrospective fixing most likely requires disassembling the magnet.

### Front and Back

Parts for this step:

```
FF
FL
BF
BL
SR
Mag50
M4hex50
```

For expectation management: calculate about 2h per ring.

1. As for [R1…8 above](#rings-18)):
    - mark the polarity of all magnets (`Mag50`) and
    - make sure that the magnet pockets in the rings and lids (`FF`, `FL`, `BF`, `BL`) are clean and chip-free before inserting the magnets
2. Insert the magnets (`Mag50`) into a ring with foot (`FF`/`BF`)
    - Handle the magnets individually and with care; they are equally as brittle as strong\
        ![separating 50mm magnets](/res/img/50mm-mag-seperation.jpg){width=30%}\
	    _separating 50mm magnets_
	- Mount `SR` using `M4hex50` for a better control when handling the magnets. Start with the innermost circle and then go outwards.\
        ![first circle of mounted magnets](/res/img/50mm-mag-first-circle.jpg){width=30%}\
	    _first circle of mounted magnets_
	- Tools like the [magnet wrench](https://gitlab.com/osii/tools/magnet-wrench) or distance pieces (as for window installation) make your life easier\
        ![using tools for mounting the 50mm magnets](/res/img/50mm-mag-tools.jpg){width=30%}\
	    _using tools for mounting the 50mm magnets_
	- In case you have to remove individual magnets again, you can push them from the back side of the ring e.g. using `M4hex50`.\
        ![removing individual magnets using a screw](/res/img/50mm-mag-removal.jpg){width=30%}\
	    _removing individual magnets using a screw_
3. Mount the lid (`FL`/`BL`) with `SR` in between; you will likely need a rubber hammer to get the parts together. Screw `M4hex50` through lid and spacer ring into the foot ring to secure the assembly.\
    ![Assembling a foot with a rubber hammer](/res/img/foot-assembly-hammer.jpg){width=30%}\
    _Assembling a foot with a rubber hammer_\
    ![foot-assembly-finished](/res/img/foot-assembly-finished.jpg){width=30%}\
    _Assembled foot_

## Mounting the Magnet

Parts for this step:

- mounted rings with lids from [this step above](#rings-18)
- mounted front and back rings with lids from [this step above](#rings-18)

as well as:

```
M4sleeve
Spacer6x6
Spacer6x2
Spacer4x6
Spacer4x2
M6lnut
M6nut
M4nut
Rod6
Rod4
```

1. Screw `M6lnut` onto the end with the short thread of all `Rod6`
2. Screw `M4sleeve` onto the end with the short thread of all `Rod4`
3. Put all rods (`Rod6`, `Rod4`) from the front through the corresponding holes of the mounted front part (`FF` | `SR` | `FL`) and lie the assembly with the front face flat on the table:\
    ![Front part with rods flat on table](/res/img/front-rods.jpg){width=30%}\
	_Front part with rods flat on table_
4. Put 2× `spacer6x2` on all `Rod6` and mount `R8` _with `RL4`_ **facing down** (the mark of `R8` oriented "upwards" (=away from the foot)
    - **NOTE** the spacer sizes differ in order to bring the magnets into the end positions as defined in the [magnet analytic text file](/src/magnet orientations/analytic-18 rings.txt)\
    ![Front part with R8+RL4 mounted](/res/img/front-rods-r8.jpg){width=30%}\
	_Front part with R8+RL4 mounted_
5. Put `spacer6x6` on all `Rod6`, `spacer4x6` on all `Rod4` and proceed with `R7`, again facing down (mark of `R7` upwards)\
    ![R7 mounted](/res/img/front-r7.jpg){width=30%}\
	_R7 mounted_
6. Repeat the prior step for `R6`…`R1` and then `R1`…`R8`. You can use [these clamps](https://gitlab.com/osii/tools/magnet-clamp/) to make your life easier.\
    ![Half of the Rings mounted](/res/img/front-r1.jpg){width=30%}\
	_Half of the Rings mounted_
7. Put `spacer6x2` on all `Rod6`, `spacer4x2` on all `Rod4` and install the mounted back part (`BL` | `SR` | `BF`). Lock the entire stack with `M6nut` and `M4nut` on the rods (`Rod6`, `Rod4`). When tightening, keep controling the lenght of the entire stack (434mm incl. `BP`)\
    ![Assembly prepared for the mounting of the back part](/res/img/mounting-back-r8.jpg){width=30%}\
	_Assembly prepared for the mounting of the back part_\
    ![Mounted back part](/res/img/mounting-back-finished.jpg){width=30%}\
	_Mounted back part_
8. Put the magnet on its feet.

For disambiguation:

![Schematic ring mounting](/res/drw/ring-mounting.svg){width=50%}\
_Schematic ring mounting_

## Shim Trays

1. Print the shim trays
	- source file: `/src/CAD/Shim Tray.par`
	- please find the STL export in the respective [release notes](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/releases)
2. Number shim trays
3. Insert shim trays

