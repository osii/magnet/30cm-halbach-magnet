# Guide for Quality Management

TBD

## Magnets

Bottom line: **Don't use deficient magnets!**

Permanent magnets come with a natural tolerance in remanence, direction of magnetization and size.
Depending on your trusted magnet supplier, these tolerances will have to be accepted (and mitigated by shimming) or controlled, when unacceptable (e.g. when receiving batches in which individual magnets show an extremely low remanence).

Other than that, common magnet flaws include chipping, demagnetization, cracks etc.
The material the magnets are made of is highly corrosive.
Consequently, **if there's _any_ suface damage, your magnet will rust!**
And you don't want that :)

So just for disambiguation: The assembly of this system requires a 100% control of all magnets.
Down the line, any flaw is likely to escalate into time-consuming repairs – or a poor image quality.

## Quality Check for Rings 

- Thickness of 20mm PP sheet! PC sheets can have higher tolerances and 15mm PP sheets (rings, ...) are milled down to 14mm. 
- Overall tolerance is ISO 2768-m (all rings)
- H9 fit (also on top!)
	> Magnets should be a tight press fit but never have to be hammered in. When turning the ring upside down the magnets shouldn't fall out. 
- magnet rotation tolerance: <1°
- Warping (as a release of inner tension) is expected when milling the rings! While this is no issue for the final assembly, it is during the milling. Make sure to fix them onto a rigid even surface (see [manufacturing instructions](/doc/manufacturing-guide.md)

### Checking Pocket Positions

If no other measurement tool than a calipar at hand, you can measure the radial distances between curved features of the magnet pockets and the bore hole (aiming for the minimal value while measuring) – and compare the measured values with reference values from the CAD model.

Here is an example template for such a measurement plan:

| Part ID | Measurement Point | CAD Value [mm] | Screenshot                                                                |
|---------|-------------------|----------------|---------------------------------------------------------------------------|
| R1      | N-1               | 24             | ![pocket measurement screenshot](/res/img/qm-pockets-r1-n1.png){width=5%} |
| R1      | N-2               | 1,77           | ![pocket measurement screenshot](/res/img/qm-pockets-r1-n2.png){width=5%} |
| R1      | N-3               | 1,95           | ![pocket measurement screenshot](/res/img/qm-pockets-r1-n3.png){width=5%} |
| R1      | O-1               | 24,46          | ![pocket measurement screenshot](/res/img/qm-pockets-r1-o1.png){width=5%} |
| R1      | O-2               | 3,22           | ![pocket measurement screenshot](/res/img/qm-pockets-r1-o2.png){width=5%} |
| R1      | O-3               | 3,02           | ![pocket measurement screenshot](/res/img/qm-pockets-r1-o3.png){width=5%} |
| R1      | S-1               | 22,52          | ![pocket measurement screenshot](/res/img/qm-pockets-r1-s1.png){width=5%} |
| R1      | S-2               | 1,58           | ![pocket measurement screenshot](/res/img/qm-pockets-r1-s2.png){width=5%} |
| R1      | S-3               | 1,82           | ![pocket measurement screenshot](/res/img/qm-pockets-r1-s3.png){width=5%} |
| R1      | W-1               | 24,47          | ![pocket measurement screenshot](/res/img/qm-pockets-r1-w1.png){width=5%} |
| R1      | W-2               | 2,85           | ![pocket measurement screenshot](/res/img/qm-pockets-r1-w2.png){width=5%} |
| R1      | W-3               | 4,42           | ![pocket measurement screenshot](/res/img/qm-pockets-r1-w3.png){width=5%} |
| R2      | N-1               | 24             | ![pocket measurement screenshot](/res/img/qm-pockets-r2-n1.png){width=5%} |
| R2      | N-2               | 1,76           | ![pocket measurement screenshot](/res/img/qm-pockets-r2-n2.png){width=5%} |
| R2      | N-3               | 1,79           | ![pocket measurement screenshot](/res/img/qm-pockets-r2-n3.png){width=5%} |
| R2      | O-1               | 24,48          | ![pocket measurement screenshot](/res/img/qm-pockets-r2-o1.png){width=5%} |
| R2      | O-2               | 3,34           | ![pocket measurement screenshot](/res/img/qm-pockets-r2-o2.png){width=5%} |
| R2      | O-3               | 3,69           | ![pocket measurement screenshot](/res/img/qm-pockets-r2-o3.png){width=5%} |
| R2      | S-1               | 22,54          | ![pocket measurement screenshot](/res/img/qm-pockets-r2-s1.png){width=5%} |
| R2      | S-2               | 1,94           | ![pocket measurement screenshot](/res/img/qm-pockets-r2-s2.png){width=5%} |
| R2      | S-3               | 1,53           | ![pocket measurement screenshot](/res/img/qm-pockets-r2-s3.png){width=5%} |
| R2      | W-1               | 24,47          | ![pocket measurement screenshot](/res/img/qm-pockets-r2-w1.png){width=5%} |
| R2      | W-2               | 3,67           | ![pocket measurement screenshot](/res/img/qm-pockets-r2-w2.png){width=5%} |
| R2      | W-3               | 3,33           | ![pocket measurement screenshot](/res/img/qm-pockets-r2-w3.png){width=5%} |
| R3      | N-1               | 22,52          | ![pocket measurement screenshot](/res/img/qm-pockets-r3-n1.png){width=5%} |
| R3      | N-2               | 3,49           | ![pocket measurement screenshot](/res/img/qm-pockets-r3-n2.png){width=5%} |
| R3      | N-3               | 3,49           | ![pocket measurement screenshot](/res/img/qm-pockets-r3-n3.png){width=5%} |
| R3      | O-1               | 24,46          | ![pocket measurement screenshot](/res/img/qm-pockets-r3-o1.png){width=5%} |
| R3      | O-2               | 1,8            | ![pocket measurement screenshot](/res/img/qm-pockets-r3-o2.png){width=5%} |
| R3      | O-3               | 1,92           | ![pocket measurement screenshot](/res/img/qm-pockets-r3-o3.png){width=5%} |
| R3      | S-1               | 23,49          | ![pocket measurement screenshot](/res/img/qm-pockets-r3-s1.png){width=5%} |
| R3      | S-2               | 2,28           | ![pocket measurement screenshot](/res/img/qm-pockets-r3-s2.png){width=5%} |
| R3      | S-3               | 1,53           | ![pocket measurement screenshot](/res/img/qm-pockets-r3-s3.png){width=5%} |
| R3      | W-1               | 24,47          | ![pocket measurement screenshot](/res/img/qm-pockets-r3-w1.png){width=5%} |
| R3      | W-2               | 1,92           | ![pocket measurement screenshot](/res/img/qm-pockets-r3-w2.png){width=5%} |
| R3      | W-3               | 1,79           | ![pocket measurement screenshot](/res/img/qm-pockets-r3-w3.png){width=5%} |

