# Changelog

## v2.1.0

- improved magnet array based on new simulation
    - further improved field homogeneity
- B<sub>0</sub>-orientation has been changed to "upwards" (normal to floor)
- magnet grade changed from N56 → N52
    - N56 magnets were found to be _very_ brittle; switching to N52 will only make a small difference to the field strength, but magnets will be hopefully less brittle and easier to handle
- improved (screw) hole positioning on rings
    - making future design changes easier (e.g. a new magnet array won't result in a reallocation of all holes anymore)
- back plate added
    - to mount gradients, shim trays and other stuff

## v2.0.0

- free-standing bore
    - only front and back rings have a foot, which gives the option to mount e.g. gradient coils outside of the magnet, resulting in a larger bore diameter; however this design still assumes inside-mounted gradient coils
- ring lids screwed instead of glued
- new ring materials
- rectangular shim tray holes
    - dropping support for [v1.0.0 shim trays](https://gitlab.com/osii/shimming/shim-trays/-/releases/v1.0.0)
- improved magnet array based on new simulation
    - moving away from classic Halbach arrangement; for better field homogeneity
- magnet grade changed from N48 → N56
    - `v1.0.0` used N48 magnets from supermagnete; the new design however required 50×12×12mm³ magnets which they don't supply so we switched to Bakker Magnetics, who also offered magnets in the N56 grade which yield ~10% more field

